# Andrew Alliance SiLA

## Description
A [SiLA](https://sila-standard.com/) compliant server that allows to run protocols on OneLab using a controlled web browser.

## Requirements

- Java 11 or above
- A recent version Firefox
- Internet connection

## Download
You can download the compiled release of the server [here](https://gitlab.com/Timort/andrew-alliance-sila/-/jobs/artifacts/master/download?job=build_and_test).

## Setup

1. Clone repository
   ```
   https://gitlab.com/Timort/andrew-alliance-sila.git
   ```
   or
   ```
   git@gitlab.com:Timort/andrew-alliance-sila.git
   ```
2. Clone submodules
    ```
    git submodule update --init -f --recursive
    ```

3. Compile
    - With test
        ```
        mvn clean install
        ```
    - Or without
        ```
        mvn clean install -DskipTests
        ```

4. If everything compiled without error you're done!

## Server Usage

1. List available network interfaces
    ```
    java -jar ./server/target/server-1.0.0-SNAPSHOT-jar-with-dependencies.jar -l
    ```

2. Start server with discovery enabled. Replace `YOUR-NET-INTERFACE` with the name of one of the interface previously listed.
    ```
    java -jar ./server/target/server-1.0.0-SNAPSHOT-jar-with-dependencies.jar -n YOUR-NET-INTERFACE
    ```

3. To stop the server type `stop`and press enter.

4. In order to save the server configuration, the -c flag must be specified like so
   ```
   -c config.json
   ```
5. By default, the web browser is not visible, to make it visible please add the following flag:
   ```
   --headless no
   ```

## Connect to the server
In order to interact with the server you'll need a client

### SiLA Browser
The easiest solution is to use the [SiLA Browser](https://sila-standard.com/dipitems/sila-2-browser/)

1. Download the jar
    ```
    https://nexus.unitelabs.ch/repository/unitelabs_base-releases/master/sila_browser.jar
    ```
2. Start the Browser
    ```
    java -jar sila_browser.jar
    ```
3. Go to http://127.0.0.1:8080/

If everything is correct, you should be able to see and interact with the SiLA Server

## Future Improvements

- Keep current session to perform action instead of login and out each time
- Expose ability to use credentials store as environment variable
- Expose a way to easily run any protocol instead of having the run hard-coded in the code
