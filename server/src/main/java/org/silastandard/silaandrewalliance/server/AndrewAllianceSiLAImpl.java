package org.silastandard.silaandrewalliance.server;

import com.google.protobuf.GeneratedMessageV3;
import io.grpc.stub.StreamObserver;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.none.andrewalliance.v1.AndrewAllianceGrpc;
import sila2.org.silastandard.none.andrewalliance.v1.AndrewAllianceOuterClass;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.types.SiLAInteger;
import sila_java.library.core.sila.types.SiLAString;
import sila_java.library.server_base.command.observable.ObservableCommandManager;
import sila_java.library.server_base.command.observable.ObservableCommandTaskRunner;

import javax.annotation.Nullable;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

/**
 * Implementation class of the Andrew Alliance SiLA Feature
 */
@Slf4j
public class AndrewAllianceSiLAImpl extends AndrewAllianceGrpc.AndrewAllianceImplBase implements AutoCloseable {
    private final ReentrantLock lock = new ReentrantLock();
    private final OneLabController oneLabController;
    private final ObservableCommandManager<
            AndrewAllianceOuterClass.RunProtocol_Parameters,
            AndrewAllianceOuterClass.RunProtocol_Responses
            > runProtocolManager;
    private final ObservableCommandManager<
            AndrewAllianceOuterClass.RunProtocolOnAnyDevice_Parameters,
            AndrewAllianceOuterClass.RunProtocolOnAnyDevice_Responses
            > runProtocolOnAnyDeviceManager;

    /**
     * Constructor
     * @param headless either or not to start the browser in headless mode
     */
    public AndrewAllianceSiLAImpl(boolean headless) {
        this.oneLabController = new OneLabController(headless);
        this.runProtocolManager = new ObservableCommandManager<>(
                new ObservableCommandTaskRunner(1, 1),
                command -> runProtocolCommand(
                        AndrewAllianceOuterClass.RunProtocol_Responses.newBuilder().build(),
                        command.getParameter().getProtocol().getValue(),
                        command.getParameter().getWorkspace().getValue(),
                        command.getParameter().getRunProtocolTimeout().getValue(),
                        command.getParameter().getDeviceId().getValue()
                ),
                oneLabController.getLongWait().multipliedBy(2)
        );
        this.runProtocolOnAnyDeviceManager = new ObservableCommandManager<>(
                new ObservableCommandTaskRunner(1, 1),
                command -> runProtocolCommand(
                        AndrewAllianceOuterClass.RunProtocolOnAnyDevice_Responses.newBuilder().build(),
                        command.getParameter().getProtocol().getValue(),
                        command.getParameter().getWorkspace().getValue(),
                        command.getParameter().getRunProtocolTimeout().getValue(),
                        null
                ),
                oneLabController.getLongWait().multipliedBy(2)
        );
    }

    /**
     * Run a protocol with an SiLA Observable command
     * @param <T> The type of the instance to return
     * @param instance The instance to return
     * @param protocol The name of the protocol to run
     * @param workspace The workspace which contains the protocol
     * @param protocolRunTimeoutSecond The maximum time before the protocol run timeout
     * @param deviceId The device on which to run the protocol.
     *                 If null any device will be used
     * @return The instance provided
     */
    private <T extends GeneratedMessageV3> T runProtocolCommand(
            final T instance, final String protocol,
            final String workspace,
            final long protocolRunTimeoutSecond,
            @Nullable final String deviceId
    ) {
        final StreamObserver<Void> streamObserver = new StreamObserver<Void>() {
            @Override
            public void onNext(Void t) { }
            @SneakyThrows
            @Override
            public void onError(Throwable throwable) {
                throw throwable;
            }
            @Override
            public void onCompleted() { }
        };
        lockedAction(streamObserver, () -> {
            try {
                this.oneLabController.setLongWait(Duration.of(protocolRunTimeoutSecond, ChronoUnit.SECONDS));
                this.oneLabController.authenticatedAction(
                        () -> oneLabController.runProtocol(protocol, workspace, deviceId)
                );
            } catch (RuntimeException e) {
                log.warn("Error while running protocol {} from {} on device {}", protocol, workspace, deviceId);
                log.warn("Exception: ", e);
                throw e;
            }
        });

        return instance;
    }

    @Override
    public void close() {
        try {
            oneLabController.close();
            runProtocolManager.close();
            runProtocolOnAnyDeviceManager.close();
        } catch (RuntimeException e) {
            log.warn("Exception occurred while closing AndrewAllianceSiLAImpl", e);
        }
    }

    @Override
    public void getActionTimeout(
            final AndrewAllianceOuterClass.GetActionTimeout_Parameters request,
            final StreamObserver<AndrewAllianceOuterClass.GetActionTimeout_Responses> responseObserver
    ) {
        responseObserver.onNext(AndrewAllianceOuterClass.GetActionTimeout_Responses
                .newBuilder()
                .setTimeout(SiLAInteger.from(oneLabController.getWait().getSeconds()))
                .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void setActionTimeout(
            AndrewAllianceOuterClass.SetActionTimeout_Parameters request,
            StreamObserver<AndrewAllianceOuterClass.SetActionTimeout_Responses> responseObserver
    ) {
        long timeoutSecond = request.getTimeout().getValue();
        if (timeoutSecond < 1) {
            throw new RuntimeException("Timeout must be greater than 1 second");
        }
        oneLabController.setWait(Duration.of(timeoutSecond, ChronoUnit.SECONDS));
        responseObserver.onNext(AndrewAllianceOuterClass.SetActionTimeout_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void setCredentials(
            final AndrewAllianceOuterClass.SetCredentials_Parameters request,
            final StreamObserver<AndrewAllianceOuterClass.SetCredentials_Responses> responseObserver
    ) {
        lockedAction(responseObserver, () -> {
            this.oneLabController.setCredentials(request.getUsername().getValue(), request.getPassword().getValue());
            log.info("Credentials set for user " + request.getUsername().getValue());
            responseObserver.onNext(AndrewAllianceOuterClass.SetCredentials_Responses.newBuilder().build());
            responseObserver.onCompleted();
        });
    }

    @Override
    public void getCurrentUser(
            final AndrewAllianceOuterClass.GetCurrentUser_Parameters request,
            final StreamObserver<AndrewAllianceOuterClass.GetCurrentUser_Responses> responseObserver
    ) {
        lockedAction(responseObserver, () -> {
            final String username = this.oneLabController.getUsername();
            if (username == null) {
                log.warn("Credentials are not set");
                responseObserver.onError(SiLAErrors.generateUndefinedExecutionError("Credentials are not set"));
            } else {
                responseObserver.onNext(
                        AndrewAllianceOuterClass.GetCurrentUser_Responses
                                .newBuilder()
                                .setUsername(SiLAString.from(username))
                                .build()
                );
                responseObserver.onCompleted();
            }
        });
    }

    @Override
    public void listProtocols(
            final AndrewAllianceOuterClass.ListProtocols_Parameters request,
            final StreamObserver<AndrewAllianceOuterClass.ListProtocols_Responses> responseObserver
    ) {
        lockedAction(responseObserver, () -> {
            try {
                this.oneLabController.authenticatedAction(() -> {
                    final Set<String> protocols = oneLabController.getProtocols(request.getWorkspace().getValue());
                    final AndrewAllianceOuterClass.ListProtocols_Responses.Builder SiLAProtocols =
                            AndrewAllianceOuterClass.ListProtocols_Responses.newBuilder().addAllProtocols(
                                    protocols.stream().map(SiLAString::from).collect(Collectors.toList())
                            );
                    responseObserver.onNext(SiLAProtocols.build());
                    responseObserver.onCompleted();
                });
            } catch (RuntimeException e) {
                responseObserver.onError(SiLAErrors.generateGenericExecutionError(e));
                log.warn("Error while listing protocols for workspace " + request.getWorkspace().getValue(), e);
            }
        });
    }

    @Override
    public void listWorkspaces(
            final AndrewAllianceOuterClass.ListWorkspaces_Parameters request,
            final StreamObserver<AndrewAllianceOuterClass.ListWorkspaces_Responses> responseObserver
    ) {
        lockedAction(responseObserver, () -> {
            try {
                this.oneLabController.authenticatedAction(() -> {
                    final Set<String> workspaces = oneLabController.getWorkspaces();
                    final AndrewAllianceOuterClass.ListWorkspaces_Responses.Builder SiLAWorkspaces =
                            AndrewAllianceOuterClass.ListWorkspaces_Responses.newBuilder().addAllWorkspaces(
                                    workspaces.stream().map(SiLAString::from).collect(Collectors.toList())
                            );
                    responseObserver.onNext(SiLAWorkspaces.build());
                    responseObserver.onCompleted();
                });
            } catch (RuntimeException e) {
                responseObserver.onError(SiLAErrors.generateGenericExecutionError(e));
                log.warn("Error while listing workspaces ", e);
            }
        });
    }

    @Override
    public void listDevices(
            final AndrewAllianceOuterClass.ListDevices_Parameters request,
            final StreamObserver<AndrewAllianceOuterClass.ListDevices_Responses> responseObserver
    ) {
        lockedAction(responseObserver, () -> {
            try {
                this.oneLabController.authenticatedAction(() -> {
                    final Set<String> devices = oneLabController.getDevices();
                    final AndrewAllianceOuterClass.ListDevices_Responses.Builder SiLADevices =
                            AndrewAllianceOuterClass.ListDevices_Responses.newBuilder().addAllDevices(
                                    devices.stream().map(SiLAString::from).collect(Collectors.toList())
                            );
                    responseObserver.onNext(SiLADevices.build());
                    responseObserver.onCompleted();
                });
            } catch (RuntimeException e) {
                responseObserver.onError(SiLAErrors.generateGenericExecutionError(e));
                log.warn("Error while listing devices", e);
            }
        });
    }

    @Override
    public void runProtocol(
            final AndrewAllianceOuterClass.RunProtocol_Parameters request,
            final StreamObserver<SiLAFramework.CommandConfirmation> responseObserver) {
        this.runProtocolManager.addCommand(request, responseObserver);
    }

    @Override
    public void runProtocolInfo(
            final SiLAFramework.CommandExecutionUUID request,
            final StreamObserver<SiLAFramework.ExecutionInfo> responseObserver
    ) {
        this.runProtocolManager.get(request).addStateObserver(responseObserver);
    }

    @Override
    public void runProtocolResult(
            final SiLAFramework.CommandExecutionUUID request,
            final StreamObserver<AndrewAllianceOuterClass.RunProtocol_Responses> responseObserver
    ) {
        this.runProtocolManager.get(request).sendResult(responseObserver);
    }

    @Override
    public void runProtocolOnAnyDevice(
            final AndrewAllianceOuterClass.RunProtocolOnAnyDevice_Parameters request,
            final StreamObserver<SiLAFramework.CommandConfirmation> responseObserver
    ) {
        this.runProtocolOnAnyDeviceManager.addCommand(request, responseObserver);
    }

    @Override
    public void runProtocolOnAnyDeviceInfo(
            final SiLAFramework.CommandExecutionUUID request,
            final StreamObserver<SiLAFramework.ExecutionInfo> responseObserver
    ) {
        this.runProtocolOnAnyDeviceManager.get(request).addStateObserver(responseObserver);
    }

    @Override
    public void runProtocolOnAnyDeviceResult(
            final SiLAFramework.CommandExecutionUUID request,
            final StreamObserver<AndrewAllianceOuterClass.RunProtocolOnAnyDevice_Responses> responseObserver
    ) {
        this.runProtocolOnAnyDeviceManager.get(request).sendResult(responseObserver);
    }

    /**
     * Use an instance lock to run a runnable and forward exception to a stream observer
     * @param streamObserver The stream observer
     * @param runnable The runnable to run
     * @param <T> The StreamObserver response type
     */
    private <T> void lockedAction(final StreamObserver<T> streamObserver, final Runnable runnable) {
        try {
            if (lock.tryLock(1, TimeUnit.SECONDS)) {
                try {
                    runnable.run();
                } finally {
                    lock.unlock();
                }
            } else {
                log.warn("Can not perform action because another command is already in progress");
                streamObserver.onError(
                        SiLAErrors.generateUndefinedExecutionError("Another command is already in progress")
                );
            }
        } catch (Exception e) {
            log.warn("Exception during locked action", e);
            streamObserver.onError(SiLAErrors.generateGenericExecutionError(e));
        }
    }
}
