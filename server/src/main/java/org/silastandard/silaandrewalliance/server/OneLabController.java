package org.silastandard.silaandrewalliance.server;

import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.annotation.Nullable;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * Class responsible for controlling a web browser using Selenium to perform various action on the OneLab web site
 * such as listing the available protocols and running one
 */
@Slf4j
public class OneLabController implements AutoCloseable {
  private static final String BASE_URL = "https://onelab.andrewalliance.com";
  private static final String LOGIN_ENDPOINT = "/login";
  private static final String LAB_ENDPOINT = "/app/lab/";
  private final WebDriver driver;
  @Getter
  private String username = null;
  private String password = null;
  @Getter @Setter
  private Duration wait = Duration.of(60, ChronoUnit.SECONDS);
  @Getter
  private Duration longWait = Duration.of(600, ChronoUnit.SECONDS);

  @FunctionalInterface
  private interface ProtocolCallback {
    boolean callback(WebElement webElement);
  }

  /**
   * Constructor
   * @param headless either or not to start the browser in headless mode
   */
  OneLabController(boolean headless) {
    WebDriverManager.firefoxdriver().setup();
    final FirefoxOptions options = new FirefoxOptions();
    options.setHeadless(headless);
    driver = new FirefoxDriver(options);
    driver.manage().window().setSize(new Dimension(1280 , 720));
    final Duration implicitPageWait = Duration.of(1, ChronoUnit.SECONDS);
    driver.manage().timeouts().implicitlyWait(implicitPageWait.getSeconds(), TimeUnit.SECONDS);
  }

  /**
   * Dispose resource managed by this instance
   */
  @Override
  public void close() {
    if (driver != null) {
      driver.quit();
    }
  }

  public void setLongWait(Duration longWait) {
    if (!longWait.isZero()) {
      this.longWait = longWait;
    } else {
      this.longWait = Duration.of(600, ChronoUnit.SECONDS);
      log.warn("Long wait duration of zero, ignoring and using default value.");
    }
  }

  /**
   * Set the OneLab credentials used to login
   * @param username The username (email)
   * @param password The password
   */
  public void setCredentials(@NonNull final String username, @NonNull final String password) {
    this.username = username;
    this.password = password;
  }

  /**
   * Perform an authenticated action by first login, running the action and logout
   * @param runnable The authenticated action to perform
   */
  public void authenticatedAction(@NonNull final Runnable runnable) {
    this.login();
    try {
      runnable.run();
    } finally {
      this.logout();
    }
  }

  /**
   * Get the workspaces available for the current logged in user
   * @return A set of workspace name
   */
  public Set<String> getWorkspaces() {
    final List<String> workspaces = getWebElementWorkspaces()
            .stream()
            .map(WebElement::getText)
            .collect(Collectors.toList());
    final Set<String> uniqueWorkspaces = new HashSet<>();
    for (final String workspace : workspaces) {
      if (uniqueWorkspaces.contains(workspace)) {
        throw new RuntimeException("Duplicate workspace: " + workspace);
      }
      uniqueWorkspaces.add(workspace);
    }
    return uniqueWorkspaces;
  }

  /**
   * Get the devices available for the current logged in user
   * @return A set of devices name (id)
   */
  public Set<String> getDevices() {
    goToUrl(BASE_URL + LAB_ENDPOINT);
    final WebDriverWait webDriverWait = newWait();
    userFriendlyError(
            () -> webDriverWait.until(ExpectedConditions.elementToBeClickable(By.id("button-nav-devices"))).click(),
            "Devices Button"
    );
    // todo add parameter to tell if we want any device name or online the ones without error with "ol-active-device-card"
    // todo do not error out if no device are available/present
    final By deviceIdLocator = By.cssSelector(".device-name");
    userFriendlyError(
            () -> webDriverWait.until(ExpectedConditions.elementToBeClickable(deviceIdLocator)),
            "Device name card"
    );
    final List<String> devices = userFriendlyError(() -> driver.findElements(deviceIdLocator), "Device name card")
            .stream()
            .map(WebElement::getText)
            .collect(Collectors.toList());
    final Set<String> uniqueDevices = new HashSet<>();
    for (final String device : devices) {
      if (uniqueDevices.contains(device)) {
        throw new RuntimeException("Duplicate device: " + device);
      }
      uniqueDevices.add(device);
    }
    return uniqueDevices;
  }

  /**
   * Get the protocols available for the current logged in user in the specified workspace name
   * @return A set of protocols
   */
  public Set<String> getProtocols(@NonNull final String workspaceName) {
    final Set<String> protocols = new HashSet<>();
    forEachProtocols(workspaceName, (element) -> {
      if (protocols.contains(element.getText())) {
        throw new RuntimeException("Duplicate protocol name: " + element.getText());
      }
      protocols.add(element.getText());
      return false;
    });
    return protocols;
  }

  /**
   * Run a protocol
   * @param protocolName The name of the protocol to run
   * @param protocolWorkspace The workspace in which the protocol is located
   * @param deviceId The device identifier to use to run the protocol.
   *                 If null, any available device will be used.
   */
  public void runProtocol(
          @NonNull final String protocolName,
          @NonNull final String protocolWorkspace,
          @Nullable final String deviceId
  ) {
    executeProtocol(protocolWorkspace, protocolName);
    // todo raise error if device not available
    //  "ol-feedback-block.warning"
    //  check if content is:
    //  The device Andrew+ is busy for the moment Select another device or wait until it is available

    final By deviceWithIdSelector = By.xpath(
            "//ol-setup-estimation/div[not(contains(@class,'disabled')) and not(contains(., ' Manual pipetting')) "
                    + ((deviceId == null) ? "" : " and contains(., '" + deviceId + "')") + "]"
    );
    final WebDriverWait webDriverWait = newWait();
    userFriendlyError(
            () -> webDriverWait.until(ExpectedConditions.presenceOfElementLocated(
                    By.xpath("//div[contains(., 'Select available experiment setup')]"))
            ),
            "Instrument selection"
    );
    // todo fixme for some reason if we don't refresh the page the instruments takes forever to become ready
    this.driver.navigate().refresh();
    userFriendlyError(
            () -> newWait().until(OneLabController::waitUntilPageLoaded),
            "Page refresh"
    );
    userFriendlyError(
            () -> webDriverWait.until(ExpectedConditions.elementToBeClickable(deviceWithIdSelector)).click(),
            "Available device with name"
    );
    userFriendlyError(
            () -> webDriverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(., 'Continue')]"))).click(),
            "Continue button"
    );
    final By dominoToCheck = By.cssSelector("ol-andrew-plus-bench-domino > .error-unchecked");
    userFriendlyError(
            () -> webDriverWait.until(ExpectedConditions.elementToBeClickable(dominoToCheck)),
            "Unchecked domino"
    );
    List<WebElement> webElements;
    do {
      silentSleep(1000); // Give some time to the website UI to update the unchecked domino
      webElements = userFriendlyError(
              () -> driver.findElements(dominoToCheck),
              "Unchecked domino"
      );
      if (!webElements.isEmpty()) {
        webElements.get(0).click();
        userFriendlyError(
                () -> webDriverWait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".volume-checked-btn"))).click(),
                "Check domino volume button"
        );
      }
    } while (!webElements.isEmpty());
    userFriendlyError(
            () -> webDriverWait.until(
                    ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(., 'Start experiment')]"))
            ).click(),
            "Button start experiment"
    );
    userFriendlyError(
            () -> webDriverWait.until(ExpectedConditions.elementToBeClickable(
                    By.xpath("//mat-dialog-container//div[contains(@class,'title') and contains(., 'Checking in progress')]"))
            ),
            "Dialog checking in progress"
    );
    final WebDriverWait longWebDriverWait = newLongWait();

    // wait until the checking in progress screen is gone
    longWebDriverWait.until((webDriver -> {
      List<WebElement> checking = webDriver.findElements(
              By.xpath("//mat-dialog-container//div[contains(@class,'title') and contains(., 'Checking in progress')]")
      );
      return checking.isEmpty();
    }));
    userFriendlyError(
            () -> webDriverWait.until(ExpectedConditions.elementToBeClickable(By.className("percent"))),
            "Percentage of run completion"
    );

    // todo progress could be retrieved from the html below
    //  <div _ngcontent-wru-c108="" class="timer"><div _ngcontent-wru-c108=""> 00:19</div></div>
    //  <div _ngcontent-wru-c108=""> 00:15</div>

    // wait until either an error occur or the run is completed
    longWebDriverWait.until((webDriver -> {
      List<WebElement> errorDialogElement = webDriver.findElements(By.cssSelector("ol-error-dialog"));
      if (!errorDialogElement.isEmpty()) {
        throw new RuntimeException("An error occurred while running the protocol " + protocolName + " please login into your OneLab account to check the error");
      }
      List<WebElement> experimentCompleted = webDriver.findElements(By.xpath("//div[contains(@class,'experiment-status') and contains(., 'Experiment completed')]"));
      return !experimentCompleted.isEmpty();
    }));
  }

  /**
   * Go to the workspace currently present on the web page with the specified name
   * @param workspaces List of workspaces
   * @param workspaceName The workspace to go to
   */
  private void goToWorkspaceOnCurrentPage(
          @NonNull final List<WebElement> workspaces,
          @NonNull final String workspaceName
  ) {
    final Optional<WebElement> optionalWorkspace = workspaces
            .stream()
            .filter(element -> element.getText().equals(workspaceName))
            .findAny();
    if (!optionalWorkspace.isPresent()) {
      throw new RuntimeException("Unable to find workspace named " + workspaceName);
    }
    userFriendlyError(
            () -> newWait().until(ExpectedConditions.elementToBeClickable(optionalWorkspace.get())).click(),
            "Named workspace button"
    );
  }

  /**
   * Go to the specified workspace name web page
   * @param workspaceName The workspace name
   */
  private void goToWorkspace(@NonNull final String workspaceName) {
    goToWorkspaceOnCurrentPage(getWebElementWorkspaces(), workspaceName);
  }

  /**
   * Iterate over each available protocols in a workspace
   * @param workspaceName The workspace name
   * @param callback The function called back for each protocol
   */
  private void forEachProtocols(@NonNull final String workspaceName, @NonNull final ProtocolCallback callback) {
    forEachProtocols(workspaceName, callback, null);
  }

  /**
   * Authenticate to OneLab using the previously set user credentials
   */
  private void login() {
    if (this.username == null || this.password == null) {
      throw new RuntimeException("User credentials are not set");
    }
    goToUrl(BASE_URL + LOGIN_ENDPOINT);
    final WebDriverWait webDriverWait = newWait();

    final By userEmailSelector = By.id("input-user-email");
    userFriendlyError(
            () -> webDriverWait.until(ExpectedConditions.elementToBeClickable(userEmailSelector)).click(),
            "User email input"
    );
    userFriendlyError(
            () -> webDriverWait.until(ExpectedConditions.elementToBeClickable(userEmailSelector)).sendKeys(this.username),
            "User email input"
    );
    final By passwordSelector = By.id("input-user-password");
    userFriendlyError(
            () -> webDriverWait.until(ExpectedConditions.elementToBeClickable(passwordSelector)).click(),
            "User password input"
    );
    userFriendlyError(
            () -> webDriverWait.until(ExpectedConditions.elementToBeClickable(passwordSelector)).sendKeys(this.password),
            "User password input"
    );

    // Get rid of the cookie banner by clicking on the ok button so the banner does not obscure web elements
    try {
      driver.findElement(By.cssSelector("ol-cookie-banner button")).click();
    } catch (RuntimeException ignored) {

    }
    userFriendlyError(
            () -> webDriverWait.until(ExpectedConditions.elementToBeClickable(By.id("button-login"))).click(),
            "Login button"
    );
    userFriendlyError(
            () -> webDriverWait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("ol-avatar"))),
            "User avatar"
    );
  }

  /**
   * Logout the current user from OneLab
   */
  private void logout() {
    goToUrl(BASE_URL + LAB_ENDPOINT);

    // Check if the user is logged in by checking the presence of the login button, if present then no need to logout
    try {
      if (driver.findElement(By.id("button-login")) != null) {
        return;
      }
    } catch (RuntimeException ignored) { }
    final WebDriverWait webDriverWait = newWait();
    userFriendlyError(
            () -> webDriverWait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("ol-avatar"))),
            "User avatar"
    );
    userFriendlyError(
            () -> webDriverWait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".right-menu"))).click(),
            "Right side user menu"
    );
    userFriendlyError(
            () -> webDriverWait.until(ExpectedConditions.elementToBeClickable(By.id("button-logout"))).click(),
            "Logout button"
    );
  }

  /**
   * Retrieve the workspace web elements
   * @return The workspace web elements
   */
  private List<WebElement> getWebElementWorkspaces() {
    goToUrl(BASE_URL + LAB_ENDPOINT);
    final WebDriverWait webDriverWait = newWait();
    userFriendlyError(
            () -> webDriverWait.until(ExpectedConditions.elementToBeClickable(By.id("button-nav-projects"))).click(),
            "Projects/Workspaces button"
    );
    final By projectNameSelector = By.cssSelector("ol-project-card .name");
    userFriendlyError(
            () -> webDriverWait.until(ExpectedConditions.elementToBeClickable(projectNameSelector)),
            "Named project card"
    );
    return userFriendlyError(
            () -> driver.findElements(projectNameSelector),
            "Named project card"
    );
  }

  /**
   * Execute the specified protocol
   * @param workspaceName The workspace name
   * @param protocolName The protocol name
   */
  private void executeProtocol(final String workspaceName, final String protocolName) {
    final WebDriverWait webDriverWait = newWait();
    final AtomicBoolean atomicBoolean = new AtomicBoolean(false);
    forEachProtocols(workspaceName, (protocol) -> {
      if (protocol.getText().equals(protocolName)) {
        atomicBoolean.set(true);
        final String currentUrl = driver.getCurrentUrl();
        userFriendlyError(
                () -> webDriverWait.until(ExpectedConditions.elementToBeClickable(By.id("button-action-execute"))).click(),
                "Execute protocol button"
        );
        userFriendlyError(
                () -> webDriverWait.until((driver) -> currentUrl.contentEquals(this.driver.getCurrentUrl())),
                "redirection to select instrument"
        );
        return true;
      }
      return false;
    }, protocolName);
    if (!atomicBoolean.get()) {
      throw new RuntimeException(
              "Unable to find protocol to execute named " + protocolName + " in workspace " + workspaceName
      );
    }
  }

  /**
   * Find protocols in a workspace and call the callback function for each protocol found
   * By default OneLab displays protocols with a limit of 10 per page.
   * The function iterates on each page to find all the protocols.
   *
   * @param workspaceName The workspace name
   * @param callback The function to callback. If the callable returns true, the iteration stop.
   * @param protocolNameToSearch The name of the protocol to search.
   *                             If null the function will retrieve all the protocols.
   */
  private void forEachProtocols(
          @NonNull final String workspaceName,
          @NonNull final ProtocolCallback callback,
          @Nullable final String protocolNameToSearch
  ) {
    goToWorkspace(workspaceName);
    final WebDriverWait webDriverWait = newWait();
    final By protocolNameSelector = By.cssSelector("ol-protocol-main-cell div > .name");
    if (protocolNameToSearch != null && !protocolNameToSearch.isEmpty()) {
      userFriendlyError(
              () -> webDriverWait.until(
                      ExpectedConditions.elementToBeClickable(By.cssSelector("ol-search-bar input"))
              ).sendKeys(protocolNameToSearch),
              "Search protocol input bar"
      );
      userFriendlyError(
              () -> webDriverWait.until(
                      ExpectedConditions.elementToBeClickable(By.cssSelector("ol-project-protocols .search-button"))
              ).click(),
              "Search protocol button"
      );
    }
    silentSleep(1000); // wait for protocols to be loaded and displayed
    boolean hasNextPage;
    do {
      userFriendlyError(
              () -> webDriverWait.until(ExpectedConditions.elementToBeClickable(protocolNameSelector)),
              "Protocol name"
      );
      final List<WebElement> elements = userFriendlyError(
              () -> driver.findElements(protocolNameSelector),
              "Protocol name"
      );
      for (WebElement element : elements) {
        if (callback.callback(element)) {
          return;
        }
      }

      // Go to the next protocols page
      final WebElement nextPageButton = userFriendlyError(
              () -> driver.findElement(By.cssSelector("button.next")),
              "Next protocol page button"
      );

      // Is there a next page available or we reached the end?
      hasNextPage = nextPageButton.isEnabled();
      if (hasNextPage) {
        nextPageButton.click();
      }
    } while (hasNextPage);
  }

  /**
   * Go to the specified URL and wait until the page is done loading
   * @param url The URL to go to
   */
  private void goToUrl(@NonNull final String url) {
    driver.get(url);
    userFriendlyError(
            () -> newWait().until(OneLabController::waitUntilPageLoaded),
            "Page loaded"
    );
  }

  /**
   * Create a waiter to wait for basic action / presence of elements
   * @return a new Waiter
   */
  private WebDriverWait newWait() {
    return new WebDriverWait(driver, this.wait.getSeconds(), 1000);
  }

  /**
   * Create a waiter to wait for long action such as running a protocol
   * @return a new long Waiter
   */
  private WebDriverWait newLongWait() {
    return new WebDriverWait(driver, this.longWait.getSeconds(), 1000);
  }

  /**
   * Waits until the page the is loaded
   * @param webDriver The webdriver
   * @return True if the page is loaded, else False.
   */
  private static boolean waitUntilPageLoaded(@NonNull final WebDriver webDriver) {
    return ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete");
  }

  /**
   * Sleep silently using a sneak throw
   * @param milliSeconds Amount of milli seconds to sleep for
   */
  @SneakyThrows
  private static void silentSleep(final int milliSeconds) {
    Thread.sleep(milliSeconds);
  }

  /**
   * Create an user friendly exception if an exception occurs while calling the callable
   * @param callable The callable to call
   * @param elementName The name of the element concerned if an error occurs
   * @param <T> Return value of the callable function
   * @return Return the return value of the callable function
   */
  private static <T> T userFriendlyError(@NonNull final Callable<T> callable, @NonNull final String elementName) {
    try {
      return callable.call();
    } catch (Exception e) {
      log.warn("Exception: ", e);
      throw new RuntimeException("The web driver was not able to find or interact with " + elementName);
    }
  }

  /**
   * Create an user friendly exception if an exception occurs while calling the runnable
   * @param runnable The runnable to run
   * @param elementName The name of the element concerned if an error occurs
   */
  private static void userFriendlyError(@NonNull final Runnable runnable, @NonNull final String elementName) {
    userFriendlyError(() -> {
      runnable.run();
      return null;
    }, elementName);
  }
}
