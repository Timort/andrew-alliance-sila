package org.silastandard.silaandrewalliance.server;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import sila_java.library.core.encryption.SelfSignedCertificate;
import sila_java.library.server_base.SiLAServer;
import sila_java.library.server_base.identification.ServerInformation;
import sila_java.library.server_base.utils.ArgumentHelper;

import java.io.IOException;

import static sila_java.library.core.utils.Utils.blockUntilStop;
import static sila_java.library.core.utils.FileUtils.getResourceContent;

/**
 * Andrew Alliance SiLA Server
 *
 */
@Slf4j
public class Server implements AutoCloseable {
    private static final String SERVER_TYPE = "Andrew Alliance";
    private static final String SERVER_DESC = "";
    private static final String SERVER_VENDOR = "https://gitlab.com/Timort/andrew-alliance-sila/";
    private static final String SERVER_VERSION = "0.1";
    private final SiLAServer server;
    private final AndrewAllianceSiLAImpl andrewAllianceSiLA;

    public static void main(final String[] args) {
        ArgumentParser customArgumentParser = ArgumentParsers.newFor(SERVER_TYPE).build()
                .defaultHelp(true)
                .description("SiLA Server.");
        customArgumentParser.addArgument("--headless")
                .type(Arguments.booleanType("yes", "no"))
                .setDefault(true)
                .help("Hide the Firefox browser window");
        final ArgumentHelper argumentHelper = new ArgumentHelper(args, SERVER_TYPE, customArgumentParser);
        if (!argumentHelper.useEncryption()) {
            log.error("Encryption must be enabled to run this server, please enable it by adding the flag '--encryption'");
            return;
        }

        try (final Server server = new Server(argumentHelper)) {
            Runtime.getRuntime().addShutdownHook(new Thread(server::close));
            blockUntilStop();
        } catch (SelfSignedCertificate.CertificateGenerationException e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void close() {
        this.server.close();
        this.andrewAllianceSiLA.close();
    }

    private Server(@NonNull final ArgumentHelper argumentHelper) throws SelfSignedCertificate.CertificateGenerationException {
        final ServerInformation serverInfo = new ServerInformation(
                SERVER_TYPE, SERVER_DESC, SERVER_VENDOR, SERVER_VERSION
        );

        try {
            final SiLAServer.Builder builder;
            if (argumentHelper.getConfigFile().isPresent()) {
                builder = SiLAServer.Builder.withConfig(argumentHelper.getConfigFile().get(), serverInfo);
            } else {
                builder = SiLAServer.Builder.withoutConfig(serverInfo);
            }
            argumentHelper.getPort().ifPresent(builder::withPort);
            argumentHelper.getInterface().ifPresent(builder::withDiscovery);
            if (argumentHelper.useEncryption()) {
                builder.withSelfSignedCertificate();
            }
            final boolean isHeadless = argumentHelper.getNs().getBoolean("headless");
            andrewAllianceSiLA = new AndrewAllianceSiLAImpl(isHeadless);
            builder.addFeature(
                    getResourceContent("andrew-alliance.sila.xml"),
                    andrewAllianceSiLA
            );
            this.server = builder.start();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
